plugins {
	groovy
	java
	`maven-publish`
	`java-gradle-plugin`
	id("io.freefair.lombok") version "6.1.0-m3"
	id("com.gradle.plugin-publish") version "0.11.0"
}

repositories {
	mavenCentral()
}

dependencies {
	implementation(gradleApi())
	implementation(localGroovy())
	implementation("org.freemarker:freemarker:2.3.20")
	implementation("com.fasterxml.jackson.core:jackson-databind:2.9.9.3")

	testImplementation("org.junit.jupiter:junit-jupiter-api:5+")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5+")
}

configure<JavaPluginConvention> {
	sourceCompatibility = JavaVersion.VERSION_11
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.register<Zip>("packageTemplates") {
	archiveFileName.set("codegen-bundle.zip")
	destinationDirectory.set(file("${project.projectDir.absolutePath}/src/main/resources"))
	from("${project.projectDir.absolutePath}/spring-boot-template")
}

//tasks.getByName("processResources").dependsOn("packageTemplates")

publishing {
	publications {
		create<MavenPublication>("plugin") {
			from(components["java"])
		}
		create<MavenPublication>("bundle") {
			artifact(tasks.getByName("packageTemplates"))
		}
	}
}

pluginBundle {
	website = "https://gitlab.com/joemielke/codegen"
	vcsUrl = "https://gitlab.com/joemielke/codegen"
	tags = listOf("spring boot", "codegen")
}

gradlePlugin {
	plugins {
		create("codegenPlugin") {
			id = "com.mielke.codegen"
			displayName = "customizable code generation"
			description = "code generation allowing the ability to setup custom project templates"
			implementationClass = "com.mielke.codegen.CodeGenPlugin"
		}
	}
}