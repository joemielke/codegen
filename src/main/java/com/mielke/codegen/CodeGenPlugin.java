package com.mielke.codegen;

import com.mielke.codegen.task.CodegenInitializer;
import com.mielke.codegen.task.CodeGen;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;

public class CodeGenPlugin implements Plugin<Project> {
    public void apply(Project project) {
        project.getConfigurations().create("codegen")
                .setDescription("override the default codegen templates");

        Task initTask = project.getTasks().create("codegenInit", CodegenInitializer.class);
        initTask.setGroup("codegen");
        Task codegenTask = project.getTasks().create("codegen", CodeGen.class).dependsOn(initTask);
        codegenTask.setGroup("codegen");
    }
}
