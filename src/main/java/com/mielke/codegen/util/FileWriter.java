package com.mielke.codegen.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileWriter {
    private static Logger logger = LoggerFactory.getLogger(FileWriter.class);

    public static boolean createFileIfNecessary(String filePath) {
        try {
            if (!Files.exists(Paths.get(filePath))) {
                Files.createDirectories(Paths.get(filePath).getParent());
                Path newPath = Files.createFile(Paths.get(filePath));
                return true;
            }
        } catch(Exception ex) {
            logger.error(ex.getMessage());
        }
        return false;
    }
}
