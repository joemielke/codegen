package com.mielke.codegen.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileReader {

    public static String readFile(String file) throws IOException {
        File initialFile = new File(file);
        InputStream targetStream = new FileInputStream(initialFile);
        String contents = convertStreamToString(targetStream);
        targetStream.close();
        return contents;
    }

    private static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
