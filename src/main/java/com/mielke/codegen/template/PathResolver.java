package com.mielke.codegen.template;

import org.gradle.api.Project;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PathResolver {

    private Project project;

    public PathResolver(Project project) {
        this.project = project;
    }

    public String getSourceFilePath(String templateRelativePath) {
        return Path.of(project.getBuildDir().getAbsolutePath(), "codegen", templateRelativePath).toString();
    }

    public String getDestinationFilePath(TemplateProcessor.TemplateInput templateInput) {
        String template = templateInput.templateFile;
        project.getLogger().debug("retrieving file path for template: {}", template);
        List<String> pathSegments = new ArrayList<>();
        pathSegments.add(project.getProjectDir().getAbsolutePath());
        Collections.addAll(pathSegments, template.split(Pattern.quote(FileSystems.getDefault().getSeparator())));
        pathSegments.removeIf(Objects::isNull);
        pathSegments.removeIf(String::isEmpty);

        String serviceName = project.getName().split("-")[0];
        String templateName = pathSegments.remove(pathSegments.size() - 1);

        pathSegments = pathSegments.stream().map(segment -> {
            if (segment.contains("_service_")) {
                return serviceName;
            }
            if (segment.contains("_group_")) {
                return project.getGroup().toString().replace(".", FileSystems.getDefault().getSeparator());
            }
            if (segment.equals("_resource_")) {
                return templateInput.resource.toLowerCase();
            }
            if (segment.contains("_resource_")) {
                return segment.replaceAll("_resource_", templateInput.resource);
            }
            if (segment.contains("_resource-lower_")) {
                return segment.replaceAll("_resource-lower_", templateInput.resource.toLowerCase());
            }
            return segment;
        }).collect(Collectors.toList());

        String fileName = templateName
                .replaceAll("_service_", capitalize(serviceName))
                .replaceAll("_resource_", capitalize(templateInput.resource))
                .replaceAll("_resource-lower_", templateInput.resource != null ? templateInput.resource.toLowerCase() : "")
                .replaceAll("_class_", "")
                .replaceAll("_file_", "")
                .replaceAll("\\.ftl", "");
        if (templateInput.filePlaceholder != null) {
            fileName = fileName.replaceAll(templateInput.filePlaceholder, "");
        }
        pathSegments.add(fileName);

        project.getLogger().debug("path segments: {}", pathSegments);
        return String.join(FileSystems.getDefault().getSeparator(), pathSegments);
    }

    private static String capitalize(String str) {
        if (str == null) {
            return "";
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}
