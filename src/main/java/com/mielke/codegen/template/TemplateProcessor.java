package com.mielke.codegen.template;

import com.mielke.codegen.util.FileReader;
import com.mielke.codegen.util.FileWriter;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import org.gradle.api.Project;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mielke.codegen.task.CodeGen.STATIC_PLACEHOLDERS;
import static com.mielke.codegen.task.CodegenInitializer.INIT_PLACEHOLDERS;

public class TemplateProcessor {

    private Project project;
    private PathResolver pathResolver;

    public static List<String> END_LINES = Arrays.asList("}", "};");

    public TemplateProcessor(Project project) {
        this.project = project;
        this.pathResolver = new PathResolver(project);
    }

    public static TemplateProcessor get(Project project) {
        return new TemplateProcessor(project);
    }

    public static class TemplateInput {
        public String templateFile;
        public String resource;
        public String operation;
        public String filePlaceholder;
    }

    public String processTemplate(String sourceTemplatePath, TemplateInput templateInput) {
        String result = null;
        try {
            Configuration cfg = new Configuration();

            StringTemplateLoader stringLoader = new StringTemplateLoader();
            cfg.setTemplateLoader(stringLoader);
            String templateString = FileReader.readFile(sourceTemplatePath);
            stringLoader.putTemplate("template", templateString);
            freemarker.template.Template template = cfg.getTemplate("template");

            Map<String, Object> input = getTemplateParameters(templateInput);
            StringWriter stringWriter = new StringWriter();
            template.process(input, stringWriter);
            project.getLogger().info(stringWriter.toString());
            result = stringWriter.toString();
        } catch (Exception ex) {
            project.getLogger().error("failed to process template: {}", ex.getMessage());
            throw new RuntimeException(ex);
        }
        return result;
    }

    private Map<String, Object> getTemplateParameters(TemplateInput templateInput) {
        Map<String, Object> input = new HashMap<>();
        input.put("resource", templateInput.resource);
        input.put("group", project.getGroup().toString().replace("/", "."));
        input.put("basePackage", project.getGroup().toString().replace("/", "."));
        input.put("service", project.getName().split("-")[0]);
        return input;
    }

    public void processTemplates(TemplateInput templateInput) throws IOException {
        Path start = Paths.get(project.getBuildDir().getAbsolutePath(), "codegen");
        if (Files.exists(start)) {
            try (Stream<Path> files = Files.walk(start)) {
                files.filter(path -> path.toString().contains(templateInput.filePlaceholder))
                        .forEach(path -> {
                            project.getLogger().info("processing path: {}", path);
                            templateInput.templateFile = start.relativize(path).toString();
                            createFileFromTemplateIfNecessary(templateInput);
                            if (!STATIC_PLACEHOLDERS.contains(templateInput.filePlaceholder) && !INIT_PLACEHOLDERS.contains(templateInput.filePlaceholder)) {
                                updateFileIfNecessary(templateInput);
                            }
                        });
            }
        }
    }

    private void createFileFromTemplateIfNecessary(TemplateProcessor.TemplateInput templateInput) {
        String filePath = pathResolver.getDestinationFilePath(templateInput);
        String sourceTemplatePath = pathResolver.getSourceFilePath(templateInput.templateFile);
        boolean created = FileWriter.createFileIfNecessary(filePath);

        if (created) {
            project.getLogger().debug("created file from template: {} at: {}", templateInput.templateFile, filePath);
            String contents = processTemplate(sourceTemplatePath, templateInput);
            try {
                Files.writeString(Path.of(filePath), contents);
            } catch (IOException ex) {
                project.getLogger().error(ex.getMessage());
            }
        }
    }

    private void updateFileIfNecessary(TemplateInput templateInput) {
        try {
            String sourceTemplatePath = pathResolver.getSourceFilePath(templateInput.templateFile);
            String processedTemplate = processTemplate(sourceTemplatePath, templateInput);
            String fileName = pathResolver.getDestinationFilePath(templateInput);
            List<String> fileLines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
            List<String> newLines = Arrays.asList(processedTemplate.split("\\r?\\n"));
            project.getLogger().debug("processed template: {}", processedTemplate);
            project.getLogger().debug("file lines: {}", fileLines);
            project.getLogger().debug("new lines: {}", newLines);
            if (fileAlreadyContainsTemplate(fileLines, newLines)) {
                return;
            }
            Collections.reverse(fileLines);
            String lastLine = fileLines.stream().filter(line -> !line.trim().isEmpty()).findFirst().orElseThrow();
            boolean reAddLastLine = false;
            if (END_LINES.contains(lastLine.trim())) {
                fileLines.remove(lastLine);
                reAddLastLine = true;
            }
            Collections.reverse(fileLines);
            fileLines.add(processedTemplate);
            if (reAddLastLine) {
                fileLines.add(lastLine);
            }
            Files.write(Paths.get(fileName), fileLines, StandardCharsets.UTF_8);
        } catch (Exception ex) {
            project.getLogger().error(ex.getMessage());
        }
    }

    private boolean fileAlreadyContainsTemplate(List<String> fileLines, List<String> newLines) {
        List<String> filteredNewLines = newLines.stream()
                .map(String::trim)
                .map(line -> line.replaceAll("\\p{C}", ""))
                .filter(line -> !line.isBlank())
                .collect(Collectors.toList());
        List<String> filteredLines = fileLines.stream()
                .map(String::trim)
                .map(line -> line.replaceAll("\\p{C}", ""))
                .filter(line -> !line.isBlank())
                .collect(Collectors.toList());
        return filteredLines.containsAll(filteredNewLines);
    }
}
