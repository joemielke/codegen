package com.mielke.codegen.task;

import com.mielke.codegen.template.TemplateProcessor;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.options.Option;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CodeGen extends DefaultTask {

    public static List<String> STATIC_PLACEHOLDERS = Arrays.asList("_class_", "_file_");

    private String resource;
    private String operation;

    private TemplateProcessor templateProcessor;

    public CodeGen() {
        templateProcessor = new TemplateProcessor(getProject());
    }

    @Input
    public String getResource() {
        return resource;
    }

    @Option(option = "resource", description = "resource name")
    public void setResource(String resource) {
        this.resource = resource;
    }

    @Input
    public String getOperation() {
        return operation;
    }

    @Option(option = "operation", description = "operation name")
    public void setOperation(String operation) {
        this.operation = operation;
    }

    @TaskAction
    void processCommand() throws IOException {
        TemplateProcessor.TemplateInput templateInput = new TemplateProcessor.TemplateInput();
        templateInput.resource = resource;
        templateInput.operation = operation;

        for (String placeholder : STATIC_PLACEHOLDERS) {
            templateInput.filePlaceholder = placeholder;
            templateProcessor.processTemplates(templateInput);
        }
        if (operation == null) {
            return;
        }
        templateInput.filePlaceholder = "_" + operation.toLowerCase() + "_";
        templateProcessor.processTemplates(templateInput);
    }
}