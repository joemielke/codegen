package com.mielke.codegen.task;

import com.mielke.codegen.template.TemplateProcessor;
import com.mielke.codegen.util.FileWriter;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class CodegenInitializer extends DefaultTask {

    public static final List<String> INIT_PLACEHOLDERS = Collections.singletonList("_init_");
    private Project project = getProject();

    @TaskAction
    void processCommand() throws IOException {
        copyFilesToBuildDir();

        TemplateProcessor.TemplateInput templateInput = new TemplateProcessor.TemplateInput();
        for (String placeholder : INIT_PLACEHOLDERS) {
            templateInput.filePlaceholder = placeholder;
            TemplateProcessor.get(project).processTemplates(templateInput);
        }
    }

    private void copyFilesToBuildDir() {
        if (Files.exists(Path.of(project.getBuildDir().getAbsolutePath(), "codegen"))) {
            project.getLogger().info("build/codegen already exists, not attempting to extract template bundle.");
            return;
        }
        Configuration config = project.getConfigurations().findByName("codegen");
        if (config != null && !config.isEmpty()) {
            extractCodegenDependency(config);
        } else {
            extractDefaultBundle();
        }
    }

    private void extractDefaultBundle() {
        if (!Files.exists(Path.of(project.getBuildDir().getAbsolutePath(), "codegen-bundle.zip"))) {
            project.getLogger().info("copying default template: codegen-bundle.zip");
            InputStream bundle = getClass().getResourceAsStream("/codegen-bundle.zip");
            FileWriter.createFileIfNecessary(project.getBuildDir().getAbsolutePath() + "/codegen-bundle.zip");
            try {
                Files.write(Paths.get(project.getBuildDir().getAbsolutePath(), "codegen-bundle.zip"), bundle.readAllBytes());
            } catch (IOException e) {
                project.getLogger().error("unable to extract default bundle", e);
            } finally {
                try {
                    bundle.close();
                } catch (IOException e) {
                    project.getLogger().error("unable to close bundle stream");
                }
            }
        }

        project.getLogger().info("extracting default template: codegen-bundle.zip");
        unzip(project.getBuildDir().getAbsolutePath() + "/codegen-bundle.zip", project.getBuildDir().getAbsolutePath() + "/codegen");
    }

    private void extractCodegenDependency(Configuration config) {
        config.forEach(file -> {
            project.getLogger().info("extracting template from dependency: {}", file);
            unzip(file.getAbsolutePath(), project.getBuildDir().getAbsolutePath() + "/codegen");
        });
    }

    private void unzip(String fileZip, String destDir) {
        try {
            byte[] buffer = new byte[1024];
            ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                if (zipEntry.isDirectory()) {
                    zipEntry = zis.getNextEntry();
                    continue;
                }
                File destFile = new File(destDir);
                File newFile = newFile(destFile, zipEntry);
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                zipEntry = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
        } catch (Exception ex) {
            project.getLogger().error("unable to unzip file: {} into: {}", fileZip, destDir);
            throw new RuntimeException(ex);
        }
    }

    private static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        FileWriter.createFileIfNecessary(destFile.getAbsolutePath());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }
}
