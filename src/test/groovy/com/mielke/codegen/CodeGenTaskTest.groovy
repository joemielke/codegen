package com.mielke.codegen

import com.mielke.codegen.task.CodeGen
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.assertTrue

class CodeGenTaskTest {

    @Test
    void canAddTaskToProject() {
        Project project = ProjectBuilder.builder().withProjectDir(new File("build/dir")).build()
        def task = project.task('codegen', type: CodeGen)
        assertTrue(task instanceof CodeGen)
    }
}
