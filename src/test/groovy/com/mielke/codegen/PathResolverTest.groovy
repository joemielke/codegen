package com.mielke.codegen

import com.mielke.codegen.template.PathResolver
import com.mielke.codegen.template.TemplateProcessor
import org.gradle.api.Project

import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.Test

import java.nio.file.FileSystems
import java.nio.file.Path

import static org.junit.jupiter.api.Assertions.assertEquals

class PathResolverTest {

    private PathResolver fileResolver
    private Project project

    @Test
    void templateResolvesToCorrectPath() {
        project = ProjectBuilder.builder()
                .withProjectDir(new File("build/dir"))
                .withName("project")
                .build()
        project.setGroup("com.group")

        fileResolver = new PathResolver(project)

        validatePath("User",
                "/src/main/java/_group_/_service_/Application_class_.java.ftl",
                "/src/main/java/com/group/project/Application.java")

        validatePath("User",
                "/src/main/java/_group_/_service_/controller/_resource_Controller_class_.java.ftl",
                "/src/main/java/com/group/project/controller/UserController.java")

        validatePath("User",
                "/src/main/java/_group_/_service_/controller/_resource__c_Controller.java.ftl",
                "/src/main/java/com/group/project/controller/UserController.java")

        validatePath("User",
                "/src/main/java/_group_/_service_/model/_resource__class_.java.ftl",
                "/src/main/java/com/group/project/model/User.java")

        validatePath("User",
                "/src/main/resources/app_file_.properties.ftl",
                "/src/main/resources/app.properties")

        validatePath("User",
                "/src/main/java/_group_/_service_/_resource_/_resource__class_.java.ftl",
                "/src/main/java/com/group/project/user/User.java")

        validatePath("User",
                "/src/main/java/_group_/_service_/_resource_/_service__resource__class_.java.ftl",
                "/src/main/java/com/group/project/user/ProjectUser.java")
    }

    void validatePath(String resource, String template, String expected) {
        String result = fileResolver.getDestinationFilePath(
                new TemplateProcessor.TemplateInput(
                        templateFile:template.replace("/", FileSystems.getDefault().getSeparator()),
                        operation: "c",
                        filePlaceholder: "_c_",
                        resource: resource))
        System.out.println(project.getProjectDir().getAbsolutePath())
        assertEquals(Path.of(project.projectDir.path,expected), Path.of(result))
    }
}
