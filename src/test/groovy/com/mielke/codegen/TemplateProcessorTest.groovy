package com.mielke.codegen

import com.mielke.codegen.template.TemplateProcessor
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TemplateProcessorTest {

    private TemplateProcessor templateProcessor
    private Project project

    @BeforeEach
    void before() {
        project = ProjectBuilder.builder()
                .withProjectDir(new File("build/dir"))
                .withName("project")
                .build()
        project.setGroup("com.group")
    }

    @Test
    void classFilesProcess() {
        templateProcessor = new TemplateProcessor(project)
        TemplateProcessor.TemplateInput templateInput = new TemplateProcessor.TemplateInput()
        templateInput.resource = "user"
        templateInput.filePlaceholder = "_class_"
        templateProcessor.processTemplates(templateInput)
    }
}
