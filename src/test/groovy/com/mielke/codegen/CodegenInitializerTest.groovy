package com.mielke.codegen

import org.gradle.internal.impldep.org.junit.rules.TemporaryFolder
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.jupiter.api.Test

import java.nio.file.Files
import java.nio.file.Path

import static org.junit.jupiter.api.Assertions.assertTrue

class CodegenInitializerTest {

    @Test
    void bundleCopied() {
        TemporaryFolder temporaryFolder = new TemporaryFolder(new File("build"))
        temporaryFolder.create()
        def buildGradle = temporaryFolder.newFile("build.gradle")
        def settingsGradle = temporaryFolder.newFile("settings.gradle")
        buildGradle << 'plugins { id "com.mielke.codegen" }\n'
        settingsGradle << 'rootProject.name = \'test\'\n'

        def runner = GradleRunner.create()
                .withArguments("codegenInit")
                .withProjectDir(temporaryFolder.root)
                .withPluginClasspath()
                .withDebug(true)
        BuildResult result = runner.build()

        assertTrue(Files.exists(Path.of(temporaryFolder.root.absolutePath, "build", "codegen-bundle.zip")))
        assertTrue(Files.isDirectory(Path.of(temporaryFolder.root.absolutePath, "build", "codegen")))
        assertTrue(Files.exists(Path.of(temporaryFolder.root.absolutePath, "build.gradle.kts")))
    }

//    @Test
//    void buildFileOverwrites() {
//        TemporaryFolder temporaryFolder = new TemporaryFolder(new File("build"))
//        temporaryFolder.create()
//        def buildGradle = temporaryFolder.newFile("build.gradle.kts")
//        def settingsGradle = temporaryFolder.newFile("settings.gradle.kts")
//        buildGradle << 'plugins { id("com.mielke.codegen") }\n'
//        settingsGradle << 'rootProject.name = "test"\n'
//
//        def runner = GradleRunner.create()
//                .withArguments("codegenInit")
//                .withProjectDir(temporaryFolder.root)
//                .withPluginClasspath()
//                .withDebug(true)
//
//        assertEquals(1, Files.readAllLines(Path.of(temporaryFolder.root.absolutePath, "build.gradle.kts")).size())
//        BuildResult result = runner.build()
//        assertTrue(Files.readAllLines(Path.of(temporaryFolder.root.absolutePath, "build.gradle.kts")).size() > 1)
//    }
}