package com.mielke.codegen

import org.gradle.api.Project
import org.gradle.internal.impldep.org.junit.rules.TemporaryFolder
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import java.nio.file.Files
import java.nio.file.Path

import static org.junit.jupiter.api.Assertions.assertTrue

class CodegenTest {
    public final TemporaryFolder temporaryFolder = new TemporaryFolder(new File("build"))
    private Project project

    @BeforeEach
    void beforeEach() {
        temporaryFolder.create()

        project = ProjectBuilder.builder()
                .withProjectDir(temporaryFolder.root)
                .withName("project")
                .build()
        project.setGroup("com.group")

        def buildGradle = temporaryFolder.newFile("build.gradle")
        def settingsGradle = temporaryFolder.newFile("settings.gradle")
        buildGradle << "plugins { id \"com.mielke.codegen\" }\ngroup = '${project.getGroup()}'"
        settingsGradle << "rootProject.name = '${project.name}'\n"
    }

    @Test
    void controllerCreated() {
        def runner = GradleRunner.create()
                .withArguments("codegen", "--operation", "c", "--resource", "ResourceOne", "--stacktrace")
                .withProjectDir(temporaryFolder.root)
                .withPluginClasspath()
                .withDebug(true)
        BuildResult result = runner.build()

        assertTrue(Files.exists(Path.of(temporaryFolder.root.absolutePath, "build.gradle.kts")))
        assertTrue(Files.exists(Path.of(temporaryFolder.root.absolutePath,
                "src", "main", "java", "com", "group", "project", "controller", "ResourceOneController.java")))
    }
}