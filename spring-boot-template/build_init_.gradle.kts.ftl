plugins {
	idea
	groovy
	application
	`maven-publish`
	id("org.springframework.boot") version "2.1.1.RELEASE"
	id("io.spring.dependency-management") version "1.0.5.RELEASE"
}

repositories {
	mavenLocal()
	mavenCentral()
}

java {
	sourceCompatibility = JavaVersion.VERSION_11
	targetCompatibility = JavaVersion.VERSION_11
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-security")

	compileOnly("org.projectlombok:lombok")
	testCompileOnly("org.projectlombok:lombok")
	annotationProcessor("org.projectlombok:lombok")
	testAnnotationProcessor("org.projectlombok:lombok:")

	testImplementation("org.spockframework:spock-core:1.3-groovy-2.4")
	testImplementation("org.spockframework:spock-spring:1.3-groovy-2.4")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.apache.httpcomponents:httpclient")
}

application {
	applicationDefaultJvmArgs = listOf("-Xmx256m")
	mainClassName = "${basePackage}.${service}.Application"
}

publishing {
	publications {
		create<MavenPublication>("bootJava") {
			artifact(tasks.getByName("bootDistZip"))
		}
	}
}