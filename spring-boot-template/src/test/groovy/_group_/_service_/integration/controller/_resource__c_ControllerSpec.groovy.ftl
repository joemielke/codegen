    def 'create ${resource}'() {
        setup:
        ${resource?cap_first} ${resource} = new ${resource?cap_first}()

        when:
        ResponseEntity<${resource?cap_first}> result = restTemplate.postForEntity("/v1/${resource}s", ${resource}, ${resource?cap_first}.class)

        then:
        result.statusCode == HttpStatus.OK
    }