package ${basePackage}.${service}.integration.controller

import ${basePackage}.${service}.integration.BaseIntegrationSpec
import ${basePackage}.${service}.model.${resource?cap_first}
import ${basePackage}.${service}.model.${resource?cap_first}Request
import org.springframework.http.ResponseEntity
import org.springframework.http.HttpStatus

class ${resource?cap_first}ControllerSpec extends BaseIntegrationSpec {
}