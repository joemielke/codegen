package ${basePackage}.${service}.integration

import ${basePackage}.${service}.Application
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [Application.class])
class BaseIntegrationSpec extends Specification {
    @Autowired
    protected TestRestTemplate restTemplate
}