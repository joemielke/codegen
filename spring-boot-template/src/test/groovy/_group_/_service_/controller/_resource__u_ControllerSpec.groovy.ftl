
    def 'update ${resource?uncap_first} - by id - success'() {
        setup:
        UUID id = UUID.randomUUID()
        ${resource?uncap_first}Service.update(_ as ${resource?cap_first}) >> { ${resource?cap_first} resource -> return resource}
        ${resource?cap_first} ${resource?uncap_first} = new ${resource?cap_first}(id: id)
        String input = mapper.writeValueAsString(${resource?uncap_first})

        when:
        MvcResult callResult = mockMvc.perform(
                MockMvcRequestBuilders.put('/v1/${resource?uncap_first}s/' + id.toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(input)
        ).andReturn()

        MockHttpServletResponse response = callResult.getResponse()

        then:
        response.status == HttpStatus.OK.value()
        ${resource?cap_first} result = mapper.readValue(response.getContentAsString(), ${resource?cap_first})
        assert result == ${resource?uncap_first}
    }