
    def 'get ${resource?uncap_first} page - search - success'() {
        setup:
        UUID id = UUID.randomUUID()
        ${resource?cap_first} ${resource?uncap_first} = new ${resource?cap_first}(id: id)
        ${resource?uncap_first}Service.search(_ as ${resource?cap_first}Request) >> { ${resource?cap_first}Request ${resource?uncap_first}Request -> return new PageImpl<>([${resource?uncap_first}]) }

        when:
        MvcResult callResult = mockMvc.perform(
                MockMvcRequestBuilders.get('/v1/${resource?uncap_first}s')
        ).andReturn()

        MockHttpServletResponse response = callResult.getResponse()

        then:
        response.status == HttpStatus.OK.value()
        HashMap<String,Object> result = mapper.readValue(response.getContentAsString(), HashMap)
        List<Object> contents = ((List)result.get("content"))
        assert contents.size() == 1
        ${resource?cap_first} result${resource?cap_first} = mapper.readValue(mapper.writeValueAsString(contents.get(0)), ${resource?cap_first})
        assert result${resource?cap_first} == ${resource?uncap_first}
    }