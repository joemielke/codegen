
    def 'delete ${resource?uncap_first} - by id - success'() {
        setup:
        UUID id = UUID.randomUUID()
        ${resource?uncap_first}Service.delete(id) >> {}

        when:
        MvcResult callResult = mockMvc.perform(
                MockMvcRequestBuilders.delete('/v1/${resource?uncap_first}s/' + id.toString())
        ).andReturn()

        MockHttpServletResponse response = callResult.getResponse()

        then:
        response.status == HttpStatus.OK.value()
    }