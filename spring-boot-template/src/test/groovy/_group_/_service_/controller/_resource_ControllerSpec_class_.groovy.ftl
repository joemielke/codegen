package ${basePackage}.${service}.controller

import com.fasterxml.jackson.databind.ObjectMapper
import ${basePackage}.${service}.model.${resource?cap_first}
import ${basePackage}.${service}.model.${resource?cap_first}Request
import ${basePackage}.${service}.service.${resource?cap_first}Service
import org.springframework.data.domain.PageImpl
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.mock.web.MockHttpServletResponse
import spock.lang.Specification

class ${resource?cap_first}ControllerSpec extends Specification {
    MockMvc mockMvc
    ${resource?cap_first}Service ${resource?uncap_first}Service
    ObjectMapper mapper = new ObjectMapper()

    def setup() {
        ${resource?uncap_first}Service = Mock()
        mockMvc = MockMvcBuilders.standaloneSetup(new ${resource?cap_first}Controller(${resource?uncap_first}Service)).build()
    }

    def 'initialize test - controller - success'() {
        when:
        MvcResult callResult = mockMvc.perform(
                MockMvcRequestBuilders.get('/v1/${resource?uncap_first}s')
        ).andReturn()

        MockHttpServletResponse response = callResult.getResponse()

        then:
        response
    }
}
