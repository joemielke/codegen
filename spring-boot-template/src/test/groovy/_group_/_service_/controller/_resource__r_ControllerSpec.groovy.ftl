    
    def 'get ${resource?uncap_first} - by id - success'() {
        setup:
        UUID id = UUID.randomUUID()
        ${resource?cap_first} ${resource?uncap_first} = new ${resource?cap_first}(id: id)
        ${resource?uncap_first}Service.get(id) >> ${resource?uncap_first}

        when:
        MvcResult callResult = mockMvc.perform(
                MockMvcRequestBuilders.get('/v1/${resource?uncap_first}s/' + id.toString())
        ).andReturn()

        MockHttpServletResponse response = callResult.getResponse()

        then:
        response.status == HttpStatus.OK.value()
        ${resource?cap_first} result = mapper.readValue(response.getContentAsString(), ${resource?cap_first})
        assert result == ${resource?uncap_first}
    }