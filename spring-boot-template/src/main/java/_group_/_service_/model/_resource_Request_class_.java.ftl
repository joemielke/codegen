package ${basePackage}.${service}.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import org.springframework.data.domain.PageRequest;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ${resource?cap_first}Request extends PageRequest {
    @SuppressWarnings("deprecation")
    public ${resource?cap_first}Request() {
        super(0,25);
    }
}