package ${basePackage}.${service}.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ${resource?cap_first}${verb?cap_first}Request {

}