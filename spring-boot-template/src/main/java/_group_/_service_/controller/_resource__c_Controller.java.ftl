
    @RequestMapping(method = RequestMethod.POST)
    public ${resource?cap_first} create(@RequestBody ${resource?cap_first} ${resource}) {
        return ${resource?uncap_first}Service.create(${resource});
    }