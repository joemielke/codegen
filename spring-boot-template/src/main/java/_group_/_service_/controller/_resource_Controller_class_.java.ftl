package ${basePackage}.${service}.controller;

import ${basePackage}.${service}.model.${resource?cap_first};
import ${basePackage}.${service}.model.${resource?cap_first}Request;
import ${basePackage}.${service}.service.${resource?cap_first}Service;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Page;

@RestController
@RequestMapping("/v1/${resource?uncap_first}s")
public class ${resource?cap_first}Controller {

    private ${resource?cap_first}Service ${resource?uncap_first}Service;

    @Autowired
    public ${resource?cap_first}Controller(${resource?cap_first}Service ${resource?uncap_first}Service) {
        this.${resource?uncap_first}Service = ${resource?uncap_first}Service;
    }
}