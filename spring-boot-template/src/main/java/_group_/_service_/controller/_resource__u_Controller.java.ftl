
    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ${resource?cap_first} update(@PathVariable UUID id, @RequestBody ${resource?cap_first} ${resource}) {
        ${resource}.setId(id);
        return ${resource?uncap_first}Service.update(${resource});
    }