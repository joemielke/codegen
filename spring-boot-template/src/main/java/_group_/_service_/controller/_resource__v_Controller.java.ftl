
    @RequestMapping(path = "/${verb}", method = RequestMethod.POST)
    public ${resource?cap_first}${verb?cap_first}Response ${verb}(@RequestBody ${resource?cap_first}${verb?cap_first}Request request) {
        return ${resource?uncap_first}Service.${verb}(request);
    }