
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ${resource?cap_first} get(@PathVariable UUID id) {
        return ${resource?uncap_first}Service.get(id);
    }