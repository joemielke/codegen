
    @RequestMapping(method = RequestMethod.GET)
    public Page<${resource?cap_first}> search(${resource?cap_first}Request request) {
        return ${resource?uncap_first}Service.search(request);
    }