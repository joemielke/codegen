
    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable UUID id) {
        ${resource?uncap_first}Service.delete(id);
    }