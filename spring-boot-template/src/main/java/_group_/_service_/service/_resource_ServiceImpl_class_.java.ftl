package ${basePackage}.${service}.service;

import ${basePackage}.${service}.model.${resource?cap_first};
import ${basePackage}.${service}.model.${resource?cap_first}Request;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ${resource?cap_first}ServiceImpl implements ${resource?cap_first}Service {
}