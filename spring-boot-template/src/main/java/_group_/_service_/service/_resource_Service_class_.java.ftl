package ${basePackage}.${service}.service;

import ${basePackage}.${service}.model.${resource?cap_first};
import ${basePackage}.${service}.model.${resource?cap_first}Request;
import java.util.UUID;
import org.springframework.data.domain.Page;

public interface ${resource?cap_first}Service {
}