
    public ${resource?cap_first}${verb?cap_first}Response ${verb?uncap_first}(${resource?cap_first}${verb?cap_first}Request request) {
        return new ${resource?cap_first}${verb?cap_first}Response();
    }