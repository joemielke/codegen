# Codegen plugin

### Usage
```
//build.gradle.kts

plugins {
  id ("com.mielke.codegen") version "0.0.1"
}

dependencies {
    // optional custom project template
    "codegen"("com.example:codegen-template:0.0.1-SNAPSHOT@zip")
    ...
}
```
After this plugin is applied, the `build/codegen` directory will contain the extracted
templates that will be used for codegen.

Run init to initialize all template files with the `_init_` placeholder in the filename. (e.g. `build_init_.gradle.kts`)

```gradlew codegenInit```

### Templates
The directory structure and templates within `build/codegen` will be used when generating code.

#### Init file name placeholder resolution
The following placeholders are replaced when running the `codegenInit` task
- `_init_` - removed from the resolved file name.

#### Path placeholder resolution
The following placeholders are replaced when resolving the path:
- `_resource_` - replaced by lower case value of the supplied `resource` option
- `_service_` - replaced by gradle `<rootProject.name>` substring to the first `'-'` character
- `_group_` - replaced by gradle `<group>`

#### Filename placeholder resolution
The following placeholders are replaced when resolving the file name:
- `_file_` - indicator for any file, removed from the resolved file name
- `_class_` - indicator for a class template, removed from the resolved file name
- `_resource_` - replaced by value of `resource`
- `_resource-lower_` - replaced by lowercase value of `resource`
- `_service_` - replaced by gradle `<rootProject.name>` substring to the first `'-'` character
- `_group_` - replaced by gradle `<group>`

#### Filename `operation` placeholders
By default underscores are placed around the `operation` name to determine
which file templates to apply.  So if you pass `--operation c`, then all files with 
`_c_` in the name will be processed.

### Custom templates

If you supply your own zip dependency using the `codegen` configuration, 
the dependency will be used to generate the templates under `build/codegen`. 

### Examples

Generate code for a `User` resource for all files with `_c_` placeholders:

`gradlew codegen --resource User --operation c`

